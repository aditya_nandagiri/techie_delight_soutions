package Java;

import com.sun.org.apache.xpath.internal.objects.XNumber;

import java.util.Scanner;

public class fibonacci {

    public static void main(String[] args) {
        Scanner inout = new Scanner(System.in);

        int number = inout.nextInt();

        for(int i=1; i<= number; i++) {
            System.out.print(printFibonacci(i)+ " ");
        }
    }

    private static int printFibonacci(int number) {

        if(number <=1) {
            return number;
        }
        return printFibonacci(number-1) + printFibonacci(number-2);

    }
}
