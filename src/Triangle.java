class Triangle extends Diagram {
    String name = "xyz";
    Triangle(double a, double b) {
        super(a, b);
    }
    // override area for triangle
    double area() {
        System.out.println("Inside Area for Triangle.");
        return dim1 * dim2 / 2;
    }
}