package leetcode.easy.arrays;

import leetcode.utils.utils;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class RemoveDuplicatesFromSortedArray {
    public static void main(String[] args) {
        int[] inputArray = utils.createIntArray();

        Map<Integer, String> filteredKeysMap = new HashMap<>();

        Integer[] outputArray = Arrays.stream(inputArray).boxed().collect(Collectors.toSet()).stream().toArray(Integer[]:: new);

        System.out.println (Arrays.toString(outputArray));

    }
}
