package arrays.algorithms.beginner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class StringLiterals {
    public static void main(String[] args)
    {
        String a = "Hello";
        String b = "Hello";

        String c = new String("Hello");

        System.out.println(a==b);
        System.out.println(a==c);

        a.concat (b);
        System.out.println(a);// prints hello
        a = a.concat (b);
        System.out.println(a); // prints hellohello



//        List<String> example = new LinkedList<String>();
//        Integer x =0;
//        while(true){
//            example.add(new String("Memory Limit Exceeded"));
//            System.out.println(++x);
//        }
    }
}
