public class Test {
    public static void main(String args[]) {
        // Diagram d = new Diagram(10, 10); // illegal now
        Rectangle r = new Rectangle(9, 5);
        Triangle t = new Triangle(10, 8);
        Diagram diagRef; // This is OK, no object is created
        diagRef = r;
        System.out.println("Area of Rectangle is: " + diagRef.area() + "Name :" +t.name);
        diagRef = t;
        System.out.println("Area of Triangle is:" + diagRef.area());
        System.out.println("Area of Rectangle is: " + r.area());
        diagRef = t;
        System.out.println("Area of Triangle is:" + t.area());
    }
}